from abc import ABC, abstractmethod


class Converter(ABC):
    @abstractmethod
    def convert(self, value):
        pass


class StringConverter(Converter):
    def convert(self, value):
        return str(value)


class IntegerConverter(Converter):
    def convert(self, value):
        return int(value)


class FloatConverter(Converter):
    def convert(self, value):
        return float(value)


class BooleanConverter(Converter):
    def convert(self, value):
        return bool(value)


class ListConverter(Converter):
    def convert(self, value):
        return list(value)
