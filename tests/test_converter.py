import pytest

from gitlab_ci_demo_project.main import StringConverter, IntegerConverter, FloatConverter, BooleanConverter, \
    ListConverter


@pytest.mark.parametrize("input_value, expected_output", [
    pytest.param("test", "test", id="string_happy"),
    pytest.param(123, "123", id="int_to_string"),
    pytest.param(12.3, "12.3", id="float_to_string"),
    pytest.param(True, "True", id="bool_to_string"),
    pytest.param([1, 2, 3], "[1, 2, 3]", id="list_to_string"),
])
def test_string_converter(input_value, expected_output):
    # Act
    result = StringConverter().convert(input_value)

    # Assert
    assert result == expected_output


@pytest.mark.parametrize("input_value, expected_output", [
    pytest.param("123", 123, id="string_to_int"),
    pytest.param(123, 123, id="int_to_int"),
    pytest.param(True, 1, id="bool_to_int"),
    pytest.param("0", 0, id="zero_string_to_int"),
])
def test_integer_converter(input_value, expected_output):
    # Act
    result = IntegerConverter().convert(input_value)

    # Assert
    assert result == expected_output


@pytest.mark.parametrize("input_value, expected_output", [
    pytest.param("123.456", 123.456, id="string_to_float"),
    pytest.param(123, 123.0, id="int_to_float"),
    pytest.param(True, 1.0, id="bool_to_float"),
    pytest.param("0.0", 0.0, id="zero_string_to_float"),
])
def test_float_converter(input_value, expected_output):
    # Act
    result = FloatConverter().convert(input_value)

    # Assert
    assert result == expected_output


@pytest.mark.parametrize("input_value, expected_output", [
    pytest.param("True", True, id="string_true_to_bool"),
    pytest.param("False", True, id="string_false_to_bool"),
    pytest.param(1, True, id="int_true_to_bool"),
    pytest.param(0, False, id="int_false_to_bool"),
    pytest.param("", False, id="empty_string_to_bool"),
])
def test_boolean_converter(input_value, expected_output):
    # Act
    result = BooleanConverter().convert(input_value)

    # Assert
    assert result == expected_output


@pytest.mark.parametrize("input_value, expected_output", [
    pytest.param("test", ['t', 'e', 's', 't'], id="string_to_list"),
    pytest.param([1, 2, 3], [1, 2, 3], id="list_to_list"),
    pytest.param((1, 2, 3), [1, 2, 3], id="tuple_to_list"),
    pytest.param(set([1, 2, 3]), [1, 2, 3], id="set_to_list"),
])
def test_list_converter(input_value, expected_output):
    # Act
    result = ListConverter().convert(input_value)

    # Assert
    assert result == expected_output
